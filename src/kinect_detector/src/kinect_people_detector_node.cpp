#include "ros/ros.h"
#include <iostream>
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/MultiArrayDimension.h"

#include <ros/console.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <vector>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/people/ground_based_people_detection_app.h>

#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>


std::vector<float> peoplePositions;

typedef pcl::PointXYZRGBA PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
pcl::PointCloud<pcl::PointXYZRGBA>::Ptr justPeoplesCloud(new pcl::PointCloud<pcl::PointXYZRGBA>);
// PCL viewer //
pcl::visualization::PCLVisualizer viewer("PCL Viewer");
pcl::visualization::PCLVisualizer person_viewer("person Viewer");
static Eigen::VectorXf ground_coeffs;
// Mutex: //
boost::mutex cloud_mutex;
pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
bool new_cloud_available_flag = false;

enum
{
  COLS = 640,
  ROWS = 480
};

class GlobalVars
{
public:
  std::string svm_filename = "/home/robot/trainedLinearSVMForPeopleDetectionWithHOG.yaml";
  float min_confidence = -5; //-1.5
  float min_cloud_size = 200; //-1.5
  float min_height = 1.4;
  float max_height = 2.3;
  float voxel_size = 0.06;
  Eigen::Matrix3f rgb_intrinsics_matrix;

  pcl::people::PersonClassifier<pcl::RGB> person_classifier;

  pcl::people::GroundBasedPeopleDetectionApp<PointT> people_detector; // people detection object

  GlobalVars()
  {
    rgb_intrinsics_matrix << 525, 0.0, 319.5, 0.0, 525, 239.5, 0.0, 0.0, 1.0; // Kinect RGB camera intrinsics
    person_classifier.loadSVMFromFile(svm_filename);                          // load trained SVM
    people_detector.setVoxelSize(voxel_size);                                 // set the voxel size
    people_detector.setIntrinsics(rgb_intrinsics_matrix);                     // set RGB camera intrinsic parameters
    people_detector.setClassifier(person_classifier);                         // set person classifier
  }
};

int processPCL();
int processGround();
int planesRemoval();

void cloud_cb(const boost::shared_ptr<const sensor_msgs::PointCloud2> &input)
{

  // ROS_DEBUG_STREAM("Received " << " pointcloud!");
  new_cloud_available_flag = true;
  peoplePositions.clear();
  // ROS_WARN_STREAM("Received " << " pointcloud!");
  static pcl::PCLPointCloud2 pcl_pc2;
  static pcl::PCLPointCloud2ConstPtr cloudPtr(&pcl_pc2);
  
  pcl_conversions::toPCL(*input, pcl_pc2);
  
  // VOXEL GRID downsampling
  std::cerr << "PointCloud after filtering: " << pcl_pc2.width * pcl_pc2.height 
       << " data points (" << pcl::getFieldsList (pcl_pc2) << ").";
  static pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
  sor.setInputCloud (cloudPtr);
  sor.setLeafSize (0.02f, 0.02f, 0.02f);
  sor.filter (pcl_pc2);

  std::cerr << "PointCloud after filtering: " << pcl_pc2.width * pcl_pc2.height 
       << " data points (" << pcl::getFieldsList (pcl_pc2) << ").";
  //END  VOXEL GRID downsampling
  
  pcl::fromPCLPointCloud2(pcl_pc2, *cloud);
  
  //do stuff with cloud here
  static int do_once = true;
  if (do_once)
  {
    processGround();
    do_once = false;
  }
  processPCL();
}

struct callback_args
{
  // structure used to pass arguments to the callback function
  PointCloudT::Ptr clicked_points_3d;
  pcl::visualization::PCLVisualizer::Ptr viewerPtr;
};
struct callback_args cb_args;

void pp_callback(const pcl::visualization::PointPickingEvent &event, void *args)
{
  struct callback_args *data = (struct callback_args *)args;
  if (event.getPointIndex() == -1)
    return;
  PointT current_point;
  event.getPoint(current_point.x, current_point.y, current_point.z);
  data->clicked_points_3d->points.push_back(current_point);
  // Draw clicked points in red:
  pcl::visualization::PointCloudColorHandlerCustom<PointT> red(data->clicked_points_3d, 255, 0, 0);
  data->viewerPtr->removePointCloud("clicked_points");
  data->viewerPtr->addPointCloud(data->clicked_points_3d, red, "clicked_points");
  data->viewerPtr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "clicked_points");
  std::cout << current_point.x << " " << current_point.y << " " << current_point.z << std::endl;
}

int processGround()
{

  pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
  viewer.addPointCloud<PointT>(cloud, rgb, "input_cloud");
  viewer.setCameraPosition(0, 0, -2, 0, -1, 0, 0);

  // Add point picking callback to viewer:

  PointCloudT::Ptr clicked_points_3d(new PointCloudT);

  cb_args.clicked_points_3d = clicked_points_3d;
  cb_args.viewerPtr = pcl::visualization::PCLVisualizer::Ptr(&viewer);
  viewer.registerPointPickingCallback(pp_callback, (void *)&cb_args);
  std::cout << "Shift+click on three floor points, then press 'Q'..." << std::endl;

  // Spin until 'Q' is pressed:
  viewer.spin();
  std::cout << "done." << std::endl;
  // Ground plane estimation:
  ground_coeffs.resize(4);
  std::vector<int> clicked_points_indices;
  for (unsigned int i = 0; i < clicked_points_3d->points.size(); i++)
    clicked_points_indices.push_back(i);
  pcl::SampleConsensusModelPlane<PointT> model_plane(clicked_points_3d);
  model_plane.computeModelCoefficients(clicked_points_indices, ground_coeffs);

  return 0;
}
int planesRemoval()
{
 

  pcl::ConditionAnd<pcl::PointXYZRGBA>::Ptr range_cond(new pcl::ConditionAnd<pcl::PointXYZRGBA>());
  range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("z", pcl::ComparisonOps::GT, 1.0)));
  range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("z", pcl::ComparisonOps::LT, 6.8)));
  // build the filter
  pcl::ConditionalRemoval<pcl::PointXYZRGBA> condrem;
  condrem.setCondition(range_cond);
  condrem.setInputCloud(cloud);
  condrem.setKeepOrganized(true);
  // apply filter
  condrem.filter(*cloud);
  return 0;
}

int processPCL()
{
  planesRemoval();
  static GlobalVars globVars;
  static unsigned count = 0;

  {
    if (new_cloud_available_flag && cloud_mutex.try_lock()) // if a new cloud is available
    {

      new_cloud_available_flag = false;

      // Perform people detection on the new cloud:
      std::vector<pcl::people::PersonCluster<PointT> > clusters; // vector containing persons clusters
      globVars.people_detector.setInputCloud(cloud);
      globVars.people_detector.setGround(ground_coeffs); // set floor coefficients
      globVars.people_detector.compute(clusters);        // perform people detection

      ground_coeffs = globVars.people_detector.getGround(); // get updated floor coefficients

      // Draw cloud and people bounding boxes in the viewer:
      viewer.removeAllPointClouds();
      viewer.removeAllShapes();
      pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
      viewer.addPointCloud<PointT>(cloud, rgb, "input_cloud");

      PointCloudT::Ptr no_ground_cloud (new PointCloudT);
      no_ground_cloud = globVars.people_detector.getNoGroundCloud();


      unsigned int k = 0;
      justPeoplesCloud->clear();
      uint32_t clust_counter = 0;
      
      for (std::vector<pcl::people::PersonCluster<PointT> >::iterator it = clusters.begin(); it != clusters.end(); ++it)
      {
        // justPeoplesCloud
        clust_counter ++;
        std::vector<int> indexes;

        ROS_WARN_STREAM ("CLUST CONF: " << it->getPersonConfidence());
        ROS_WARN_STREAM ("CLUST SIZE: " << it->getNumberPoints());
        ROS_WARN_STREAM ("CLUST SIZE: " << it->getNumberPoints());
        ROS_WARN_STREAM ("");
        if ((it->getPersonConfidence() > globVars.min_confidence) && (it->getNumberPoints() > globVars.min_cloud_size) &&(it->getHeight()> globVars.min_height)) // draw only people with confidence above a threshold
        {

          ROS_WARN_STREAM("it->getNumberPoints " <<  it->getNumberPoints());
          Eigen::Vector3f minCoords = it->getMin();
          Eigen::Vector3f maxCoords = it->getMax();
          
          peoplePositions.push_back(minCoords[0]);
          peoplePositions.push_back(minCoords[1]);
          peoplePositions.push_back(minCoords[2]);
          
          peoplePositions.push_back(maxCoords[0]);
          peoplePositions.push_back(maxCoords[1]);
          peoplePositions.push_back(maxCoords[2]);
          
          
          // SHOWING JUST CLUSTER
            if (0) // TODO: ADD ros parameter here
            {
              pcl::PointIndices inliers = it->getIndices();
              std::vector<int> indices = inliers.indices;
              for(unsigned int i = 0; i < indices.size(); i++)        // fill cluster cloud
              {
                PointT* p = &no_ground_cloud->points[indices[i]];
                justPeoplesCloud->push_back(*p);
              }
            }
            // SHOWING  CLUSTER + NEAREST SPACE
            if (0)// TODO: ADD ros parameter here
            {
                pcl::ConditionAnd<pcl::PointXYZRGBA>::Ptr range_cond(new pcl::ConditionAnd<pcl::PointXYZRGBA>());
                range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("z", pcl::ComparisonOps::GT, minCoords[2]-0.2)));
                range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("z", pcl::ComparisonOps::LT, maxCoords[2]+0.2)));
                range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("x", pcl::ComparisonOps::GT, minCoords[0]-0.2)));
                range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("x", pcl::ComparisonOps::LT, maxCoords[0]+0.2)));
                range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("y", pcl::ComparisonOps::GT, minCoords[1]-0.2)));
                range_cond->addComparison(pcl::FieldComparison<pcl::PointXYZRGBA>::ConstPtr(new pcl::FieldComparison<pcl::PointXYZRGBA>("y", pcl::ComparisonOps::LT, maxCoords[1]+0.2)));
                // build the filter
                pcl::ConditionalRemoval<pcl::PointXYZRGBA> condrem;
                condrem.setCondition(range_cond);
                condrem.setInputCloud(cloud);
                condrem.setKeepOrganized(true);
                // apply filter
                condrem.filter(*cloud);
                viewer.removeAllPointClouds();
                viewer.removeAllShapes();
                pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
                viewer.addPointCloud<PointT>(cloud, rgb, "input_cloud");
            }
          
          ROS_INFO_STREAM("Person Coords min: " << float(minCoords[0]) <<" " << float(minCoords[1])<< " " <<float(minCoords[2])<< " " << "   , Person Coords max: " << it->getMax());
          // ROS_INFO_STREAM("Person Coords min: " >> x);
          it->drawTBoundingBox(viewer, k);
          k++;
          
        }
        ROS_WARN_STREAM("NUMB OF CLUSTERS: " <<clust_counter);
      }

      // person_viewer

      person_viewer.removeAllPointClouds();
      person_viewer.removeAllShapes();
      pcl::visualization::PointCloudColorHandlerRGBField<PointT> peopleRgb(justPeoplesCloud);
      person_viewer.addPointCloud<PointT> (justPeoplesCloud, peopleRgb, "cluster_cloud");
      person_viewer.setCameraPosition(0, 0, -2, 0, -1, 0, 0);
      person_viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "cluster_cloud");
      std::cout << k << " people found" << std::endl;
      viewer.spinOnce();
      person_viewer.spinOnce();

      // Display average framerate:
      if (++count == 30)
      {
        // double now = pcl::getTime ();
        // std::cout << "Average framerate: " << double(count)/double(now - last) << " Hz" <<  std::endl;
        count = 0;
        // last = now;
      }
      cloud_mutex.unlock();
    }
  }

  return 0;
}

int main(int argc, char **argv)
{
  cloud_mutex.unlock();
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("/kinect2/sd/points", 1, cloud_cb);
  ros::Rate r(10); // 10 hz
  ros::Publisher people_pub = n.advertise<std_msgs::Float32MultiArray >("/detected_people_positions", 1);
  ros::Publisher people_pub_cloud = n.advertise<sensor_msgs::PointCloud2 >("/detected_people_positions_point_cloud", 1);
  
  // pcl::PointCloud<PointT>::Ptr msgCloud (new pcl::PointCloud<PointT>);
  // msgCloud->header.frame_id = "some_tf_frame";
  // msgCloud->height = msgCloud->width = 1;
  // msgCloud->points.push_back (PointT());
  
  while (ros::ok())
  {
    // processPCL();
    ros::spinOnce();
    
    r.sleep();
    std_msgs::Float32MultiArray msg;
    if (peoplePositions.size()> 0)
    {
      
      msg.data.clear();
      std_msgs::MultiArrayDimension dim[1];
      dim[0].label  = "[x0,y0,z0, x1,y1,z1],[...], ... TBoundingBox of each person";
      dim[0].size = peoplePositions.size();
      msg.layout.dim.push_back(dim[0]);
      // msg.layout = ;
      for(auto & position : peoplePositions)
        {
          msg.data.push_back(position);
        }
    }
    people_pub.publish(msg);
    // pcl_conversions::toPCL(ros::Time::now(), msgCloud->header.stamp);
    sensor_msgs::PointCloud2 output;
    
    pcl::PCLPointCloud2 point_cloud2;
    pcl::toPCLPointCloud2(*cloud, point_cloud2);
    pcl_conversions::fromPCL(point_cloud2, output);
    output.header.frame_id = "kinect2_ir_optical_frame";
    people_pub_cloud.publish (output);
  }

  // ros::spinOnce();
}